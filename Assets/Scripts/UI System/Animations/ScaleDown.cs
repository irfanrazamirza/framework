﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScaleDown : UIHideAnimation
{
    RectTransform rectTransform;
    Vector3 currentScale;
    public override void Animate(BaseUI baseUI)
    {
        base.Animate(baseUI);
    }
    public override void OnAnimationRunning(BaseUI baseUI, float perc)
    {
        rectTransform.localScale = currentScale * (1 - perc);
        currentScale = rectTransform.localScale;
    }
    public override void OnAnimationStart(BaseUI baseUI)
    {
        base.OnAnimationStart(baseUI);
        rectTransform = baseUI.content.GetComponent<RectTransform>();
        currentScale = rectTransform.localScale;
    }
    public override void OnAnimationEnd(BaseUI baseUI)
    {
        rectTransform.localScale = Vector3.zero;
        base.OnAnimationEnd(baseUI);
    }
}
