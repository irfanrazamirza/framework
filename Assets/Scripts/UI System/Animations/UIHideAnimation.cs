﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UIHideAnimation : ScriptableObject 
{
	[SerializeField] public float animationTime;
    [SerializeField] public AnimationCurve animationCurve;
	[SerializeField] public bool isAnimationRunning;
	protected IEnumerator animation;
    
    public virtual void Animate(BaseUI baseUI)
    {
        isAnimationRunning = false;
        baseUI.GetComponent<Canvas>().enabled = true;
        animation = Animation(baseUI);
        baseUI.StartCoroutine(animation);
    }
    public virtual IEnumerator Animation(BaseUI baseUI)
    {
        float elapsed = 0;
		float perc;
        OnAnimationStart(baseUI);
        while (elapsed < animationTime)
        {
            perc = elapsed / animationTime;
            OnAnimationRunning(baseUI, animationCurve.Evaluate(perc));
            elapsed += Time.deltaTime;
            yield return 0f;
        }
        OnAnimationEnd(baseUI);
        yield return 0f;
    }

    public virtual void OnAnimationStart(BaseUI baseUI)
    {
		isAnimationRunning = true;
    }

    public virtual void OnAnimationEnd(BaseUI baseUI)
    {
        if(animation!=null)
        baseUI.StopCoroutine(animation);

		isAnimationRunning = false;
        baseUI.Disable();
        baseUI.OnShowEnded.Invoke();
    }
    public virtual void OnAnimationRunning(BaseUI baseUI, float perc)
    {
        
    }
}
