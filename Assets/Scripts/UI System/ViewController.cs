﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Games.UI
{
    public enum ScreenEnum
    {
        MainMenu,
        GamePlay,
        GameOver
    }
   public class ViewController : Singleton<ViewController>
    {
        [SerializeField]
        Popup globalPopup;
        [SerializeField]
        Screen currentView;
        [SerializeField]
        Screen previousView;

        [SerializeField] List<ScreenView> screens = new List<ScreenView>();
        [System.Serializable]
        public struct ScreenView
        {
            public Screen screen;
            public ScreenNames screenName;
        }

        void Start()
        {
            Init();
        }
        public void ChangeView(ScreenNames screen)
        {
            if (currentView != null)
            {
                if (previousView)
                    previousView.EndHideAnimation();
                previousView = currentView;
                previousView.Hide();
                currentView = screens[GetScreen(screen)].screen;
                currentView.Show();
            }
            else
            {
                currentView = screens[GetScreen(screen)].screen;
                currentView.Show();
            }
        }
        int GetScreen(ScreenNames screen)
        {
            return screens.FindIndex(
            delegate (ScreenView screenView)
            {
                return screenView.screenName.Equals(screen);
            });
        }
        public void RedrawView()
        {
            currentView.Redraw();
        }
        private void Init()
        {
            for (int indexOfScreen = 0; indexOfScreen < screens.Count; indexOfScreen++)
            {
                screens[indexOfScreen].screen.Disable();
            }
            ChangeView(ScreenNames.LoadingScreen);
            Input.multiTouchEnabled = false;
        }
        public void ShowPopup(string title, string description)
        {
            globalPopup.Show(title, description);
        }
        public void HidePopup()
        {
            globalPopup.Hide();
        }
    }
}

