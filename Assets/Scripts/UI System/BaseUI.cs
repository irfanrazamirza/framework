﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class BaseUI : MonoBehaviour
{
    [HideInInspector] public GameObject content;
    protected Canvas canvas;
    [SerializeField] private UIShowAnimation showAnimation;
    [SerializeField] private UIHideAnimation hideAnimation;

    [SerializeField] public UnityEvent OnShowEnded;
    [SerializeField] public UnityEvent OnHideEnded;

    public virtual void Awake()
    {
        content = transform.GetChild(0).gameObject;
        canvas = GetComponent<Canvas>();
    }

    public virtual void Disable()
    {
        canvas.enabled = false;
    }
    public virtual void Show()
    {
        if (showAnimation)
        {
            EndHideAnimation();
            showAnimation.Animate(this);
        }
        else
        {
            canvas.enabled = true;
        }
        Redraw();
    }
    public virtual void Hide()
    {
        if (hideAnimation)
        {
            EndShowAnimation();
            hideAnimation.Animate(this);
        }
        else
        {
            canvas.enabled = false;
        }
    }
    public void EndHideAnimation()
    {
        if (hideAnimation)
        {
            if (hideAnimation.isAnimationRunning)
            {
                hideAnimation.OnAnimationEnd(this);
            }
        }
    }
    public void EndShowAnimation()
    {
        if (showAnimation)
        {
            if (showAnimation.isAnimationRunning)
            {
                showAnimation.OnAnimationEnd(this);
            }
        }
    }

    public virtual void Redraw()
    {

    }
}

