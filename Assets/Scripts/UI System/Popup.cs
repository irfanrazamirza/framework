﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup : BaseUI
{
    public virtual void Show(string title, string description)
    {
        if (showAnimation)
        {
            showAnimation.Animate(this);
        }
        else
        {
            base.canvas.enabled = true;

        }
    }

    public override void Redraw()
    {
        base.Redraw();
    }
    public virtual void Close()
    {

    }
}
