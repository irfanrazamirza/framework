﻿using UnityEngine;

public class PersistentData : ScriptableObject
{

    public int highScore;
    public float totalPlayTime;

    public RuntimeData runtimeData;
    
    public void AssignData()
    {
        runtimeData.currentHighScore = highScore;
        runtimeData.currentTotalPlayTime = totalPlayTime;
    }

    public void GetData()
    {
        highScore = runtimeData.currentHighScore;
        totalPlayTime = runtimeData.currentTotalPlayTime;
    }

}
