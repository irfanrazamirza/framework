﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class DataManager : MonoBehaviour
{
    public PersistentData data;

    string path, jsonString;
    [ContextMenu("Load")]
    public void Load()
    {
        path = Application.dataPath + "/gameData.json";
        if (File.Exists(path))
        {
            jsonString = File.ReadAllText(path);
            JsonUtility.FromJsonOverwrite(jsonString,data);
        }
        else
        {
            Debug.Log("No file.");
        }

        data.AssignData();
    }

    [ContextMenu("Save")]
    public void Save()
    {
        data.GetData();
        path = Application.dataPath + "/gameData.json";
        if (File.Exists(path))
        {
            jsonString = JsonUtility.ToJson(data);
            File.WriteAllText(path, jsonString);
            

        }
        else
        {
            // File.Create(path);
            jsonString = JsonUtility.ToJson(data);
            File.WriteAllText(path, jsonString);
        }

        

    }
}
