﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RuntimeData : ScriptableObject
{
    public int currentHighScore;
    public float currentTotalPlayTime;
}
