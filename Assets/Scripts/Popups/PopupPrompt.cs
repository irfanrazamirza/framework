﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Games.UI
{
    public class PopupPrompt : Popup
    {
        public Text title;
        public Text description;
        
        public override void Show(string title, string description)
        {
            this.title.text = title;
            this.description.text = description;
            base.Show(title, description);
        }
        public override void Hide()
        {
            base.Hide();
        }
        public override void Redraw()
        {
            base.Redraw();

        }

        public void OnOkay()
        {
            ViewController.Instance.HidePopup();
        }
        
    }
}
