﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Games.UI
{
    public class PopupHealth : Popup
    {
        public override void Show()
        {
            base.Show();
        }
        public override void Hide()
        {
            base.Hide();
        }
        public override void Redraw()
        {
            base.Redraw();

        }        

		public void OnClose()
		{
            
            ViewController.Instance.Hide(this);
			// Hide();
		}
    }
}
