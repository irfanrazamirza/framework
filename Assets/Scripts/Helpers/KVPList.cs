﻿using System.Collections.Generic;
[System.Serializable]
public class KVPList<Tkey,TValue> : List<KeyValuePair<Tkey,TValue>>
{
    public void Add(Tkey key, TValue value)
    {
        base.Add(new KeyValuePair<Tkey, TValue>(key, value));
    }
}