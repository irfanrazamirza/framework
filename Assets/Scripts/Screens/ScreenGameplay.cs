﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Games.UI
{
    public class ScreenGameplay : Screen
    {
        public override void Show()
        {
            base.Show();
        }
        public override void Hide()
        {
            base.Hide();
        }
        public override void Redraw()
        {
            base.Redraw();
        }

        //button callbacks
        public void OnGameover()
        {
            ViewController.Instance.Show(ScreenEnum.GameOver);
        }

    }
}
