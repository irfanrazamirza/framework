﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Games.UI
{
    public class ScreenGameOver : Screen
    {
        public Popup healthPopup;

        public override void Show()
        {
            base.Show();
        }
        public override void Hide()
        {
            base.Hide();
        }
        public override void Redraw()
        {
            base.Redraw();
        }

        //button callbacks
        public void OnMainMenu()
        {
            ViewController.Instance.Show(ScreenEnum.MainMenu);
        }


        public void OnShowHealthPopup()
        {
            ViewController.Instance.Show(healthPopup);
            // healthPopup.Show();
        }
    }
}
