﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace Games.UI
{
    public class ScreenMainMenu : Screen
    {
        public override void Show()
        {
            base.Show();
        }
        public override void Hide()
        {
            base.Hide();

        }
        public override void Redraw()
        {
            base.Redraw();
        }
    
        //button callbacks
        public void OnGameplay()
        {
            ViewController.Instance.Show(ScreenEnum.GamePlay);
        }
    }
}