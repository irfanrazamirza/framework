﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Divider))]
public class DividerDrawer : DecoratorDrawer
{
	public override void OnGUI(Rect position)
	{
		position.y-=2;
		GUI.Label(position,"",new GUIStyle(GUI.skin.horizontalSlider));
	}
}

