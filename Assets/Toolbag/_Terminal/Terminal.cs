﻿/* Original script by: https://www.reddit.com/user/danokablamo
https://www.reddit.com/user/notrorschach added features like

"autocomplete", 
"run last command", 
"preserve the terminal across all scenes"

and some minor tweaks to make it more universal.


GETTING STARTED:

Just add the TerminalCanvas prefab to your first scene. 
The Canvas is preserved across all scenes so you can bring 
it up in every scene without the need of putting it separately
in each scene. 

There's one small caveat though - since the input
field and essentially the whole terminal resides in a UICanvas,
these should be an EventSystem in every scene you want to use it.
A workaround is to put an EventSystem in every scene. That should
do it.

COMMANDS:

It comes with preloaded commands like:

-load #    - loads the # scene (# = number of the scene in your build settings)
-reload    - reloads the current scene
-slow      - halves the timeScale
-fast      - doubles the timeScale
-speed #   - multiplies the timeScale by #
-reset     - resets the timeScale
-quit      - exists the terminal

HOW TO USE:

The input field takes a maximum of two parameters separated by a space.

For example, the command "load 2" will load the scene 2. The first parameter
is the command and the second one is the data the command should operate on.

The command "fast" only takes on parameter in the input. 

- Press the tilde "`" to bring up the terminal field.
- Press enter/return on your keyboard ot execute the command.
- Press the "up arrow" to display the last used command
- Press the "right arrow" to use the command suggested by autocomplete


CUSTOMISING:

It goes without saying that you'll need to add your own commands to make the
terminal suit better to your project. You can do it in two simple steps.

1. Add a case in the Execute() method. The name of the case should be your 
command name.

2. Add the command name to the AddCommands() method. This will help the autocomplete
feature include your command in the execution.

*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Terminal : MonoBehaviour 
{
	List<string> commands = new List<string>();
	List<string> found= new List<string>();

	Text autocompleteText;
	bool terminalActive;
	string terminalInput;
	InputField terminalInputField;
	string[] strArray;
	public GameObject TerminalGO;

	void Start()
	{

		//deactivate the terminal
		terminalActive = false;
		TerminalGO.SetActive(false);

		//get references
		autocompleteText = TerminalGO.transform.GetChild(2).GetComponent<Text>();
		terminalInputField = TerminalGO.GetComponent<InputField>();

		//add commands to the list
		commands.Add("load");
		commands.Add("reload");
		commands.Add("slow");
		commands.Add("fast");
		commands.Add("speed");
		commands.Add("reset");
		commands.Add("help");

	}

	public void OnType()
	{

		found = commands.FindAll( w => w.StartsWith(terminalInputField.text));
		if (found.Count > 0)
		{
			autocompleteText.text = found[0];
		}	

		else
		{
			autocompleteText.text="";

		}

		if(terminalInputField.text.Equals(""))
		{
			autocompleteText.text="";
		}
	}

	void Update()
	{

		InputProcessor();
	}

	void InputProcessor()
	{

		if(Input.anyKeyDown)
		{
			if(terminalActive)
			{
				List<string> found = commands.FindAll( w => w.StartsWith(terminalInputField.text));
				if (found.Count > 0 && Input.GetKeyDown(KeyCode.Tab))
				{
					terminalInputField.text = found[0]+" ";
					terminalInputField.MoveTextEnd(false);

				}
			}
		}

		if(Input.GetKeyDown(KeyCode.BackQuote))
		{
			terminalActive = !terminalActive;
			StartTerminal(terminalActive);
		}   

		if(Input.GetKeyDown(KeyCode.Return))
		{
			//check if a suggestion is available
			//if yes, make it the command

			if (found.Count > 0)
			{
				terminalInputField.text = found[0];
			}	

			ProcessInput();
		}

		if(terminalActive && Input.GetKeyDown(KeyCode.UpArrow))
		{
			
			terminalInputField.text = PlayerPrefs.GetString("lastCommand", "");
			terminalInputField.MoveTextEnd(false);
		}

		if(Input.GetKeyDown(KeyCode.Escape) && terminalActive)
		{
			DeactivateTerminal();
		}
	}

	void ClearConsole()
	{
		var logEntries = System.Type.GetType("UnityEditorInternal.LogEntries,UnityEditor.dll");
		var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
		clearMethod.Invoke(null,null);
	}

	void StartTerminal(bool _terminalActiveStatus)
	{
		if(_terminalActiveStatus = true)
		{
			ClearConsole();
			TerminalGO.SetActive(true);
			UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(terminalInputField.gameObject, null);
			terminalInputField.OnPointerClick(new UnityEngine.EventSystems.PointerEventData(UnityEngine.EventSystems.EventSystem.current));
			Cursor.visible = true;
			
		}

		else
		{
			Cursor.visible = false;
		}

		terminalActive = _terminalActiveStatus;
	}

	void ProcessInput()
	{
		
		//store input
		string input = terminalInputField.text;

		if(input.Length<1)

		return;

		//remember this command
		PlayerPrefs.SetString("lastCommand", input);
		//break the input into two pieces separating at the space
		strArray = terminalInputField.text.Split(" "[0]);

		//store the first part as the command
		string command = strArray[0];

		//store the second part (if it exists) as a value that 
		string value = "";
		if(strArray.Length > 1)
		{
			value = strArray[1];
		}

		//clean the input field
		terminalInputField.text = "";
	
		Execute(command, value);

		DeactivateTerminal();

	
	}

	void DeactivateTerminal()
	{
		TerminalGO.SetActive(false);
		terminalActive = false;
	}

	void Execute(string _command, string _value)
	{
		_command = _command.ToLower();
		//Add whatever commands you want to in after the case.
		switch(_command)
		{

		case "load":

			Debug.Log("Loading scene "+_value);
			Application.LoadLevel(int.Parse(_value));
			break;

		case "reload":

			Debug.Log("Reloading");
			Application.LoadLevel(Application.loadedLevel);
			break;

		case "fast":
			Time.timeScale = 2*Time.timeScale;
			Debug.Log(Time.timeScale+"x speed");
			break;

		case "slow":
			Time.timeScale = Time.timeScale/2;
			Debug.Log(Time.timeScale+"x speed");
			break;

		case "speed":
			Time.timeScale =int.Parse(_value);
			print(Time.timeScale+"x speed");
			break;

		case "reset":
			Time.timeScale = 1;
			print(Time.timeScale+"x speed");		
			break;


		case "help":
			string allCommands="";
			for(int i=0; i<commands.Count; i++)
			{
				allCommands=allCommands+commands[i]+", ";
			}

			//remove the extra space and comma	
			allCommands = allCommands.Remove(allCommands.Length-2, 2);

			Debug.Log(allCommands);
			break;

		case "upupdowndownleftrightleftrightba":
			Debug.Log("KONAMIC CODE ENTERED!");
			break;

		default:
			Debug.Log("Invalid command");
			break;
		}
	}
}

