﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.Text;
using System.Text.RegularExpressions;

public static class Helper
{
    public static Coroutine Execute(this MonoBehaviour monoBehaviour, Action action, float time)
    {
        return monoBehaviour.StartCoroutine(DelayedAction(action, time));
    }

    static IEnumerator DelayedAction(Action action, float time)
    {
        yield return new WaitForSeconds(time);

        action();
    }

    public static T ToEnum<T>(this string value)
    {
        return (T)Enum.Parse(typeof(T), value, true);
    }

    /// <summary>
    /// Extension method to return an enum value of type T for the given int.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this int value)
    {
        var name = Enum.GetName(typeof(T), value);
        return name.ToEnum<T>();
    }

    /// <summary>
    /// Invokes an action after waiting for a specified number of seconds
    /// </summary>
    /// <param name="delay">The number of seconds to wait before invoking</param>
    /// <param name="myAction">The action to invoke after the time has passed</param>
    public static IEnumerator InvokeInSeconds(float delay, Action myAction)
    {
        yield return new WaitForSeconds(delay);
        myAction();
    }

    //	StartCoroutine(Helper.Schedule(
    //		new Action[]
    //		{
    //			()=>print("2 seconds"),
    //			()=>print("0 seconds"),
    //			()=>print("5 seconds")
    //		},
    //
    //		new float[]
    //		{
    //			2,0,5
    //		}
    //		));
    /// <summary>
    /// Schedule the specified actions.`
    /// </summary>
    /// <param name="myActions">Array of actions to be performed.</param>
    /// <param name="delay">The delay betweek two consecutive actions.</param>
    public static IEnumerator Schedule(Action[] myActions, float[] delay)
    {
        for (int i = 0; i < myActions.Length; i++)
        {
            yield return new WaitForSeconds(delay[i]);
            myActions[i]();
        }
    }

    /// <summary>
    /// Map the specified current1, current2, target1, target2 and val.
    /// </summary>
    /// <param name="current1">Lower bound of the value's current range.</param>
    /// <param name="current2">Upper bound of the value's current range.</param>
    /// <param name="target1">Lower bound of the value's target range.</param>
    /// <param name="target2">Upper bound of the value's target range.</param>
    /// <param name="val">Value to be scaled or mapped.</param>
    public static float Map(float current1, float current2, float target1, float target2, float val)
    {
        //third parameter is the interpolant between the current range which, in turn, is used in the linear interpolation of the target range. 
        return Mathf.Lerp(target1, target2, Mathf.InverseLerp(current1, current2, val));
    }


    public static void SafeDestroy(this GameObject go)
    {
        if (go != null)
        {
            MonoBehaviour.Destroy(go);
        }
        else
        {
            Debug.Log("The object that you're trying to destroy doesn't exist!");
        }
    }

    /// <summary>
    /// Converts letter to its ASCII equivalent.
    /// </summary>
    /// <returns>ASCII value of the letter.</returns>
    /// <param name="str">String sent.</param>
    /// <param name="index">Index of char in the string to convert, 0 by default.</param>
    public static int LetterToASCII(this string str, int index = 0)
    {
        char i = str[index];
        int j = i;
        return j;
    }

    //if(!SanePrefs.isBonusMiniGame) helper
    public enum Axis { X, Y, Z, XY, XZ, YZ, ALL }
    /// <summary>
    /// Transform to apply if(!SanePrefs.isBonusMiniGame) to.
    /// </summary>
    /// <param name="transform">Transform of the gameObject.</param>
    /// <param name="duration">Duration.</param>
    /// <param name="magnitude">Magnitude.</param>
    /// <param name="axis">Axis.</param>
    /// <param name="usePause">If set to <c>true</c> use pause.</param>
    public static IEnumerator Shake(this Transform transform, float duration, float magnitude, Axis axis, bool usePause = true)
    {
        for (float time = 0; time < duration; time += usePause ? Time.deltaTime : Time.unscaledDeltaTime)
        {
            if (!usePause || Time.timeScale != 0)
            {
                Vector3 offset = new Vector3(
                    axis == Axis.X || axis == Axis.XY || axis == Axis.XZ || axis == Axis.ALL ? UnityEngine.Random.Range(-magnitude, magnitude) : 0,
                    axis == Axis.Y || axis == Axis.XY || axis == Axis.YZ || axis == Axis.ALL ? UnityEngine.Random.Range(-magnitude, magnitude) : 0,
                    axis == Axis.Z || axis == Axis.XZ || axis == Axis.YZ || axis == Axis.ALL ? UnityEngine.Random.Range(-magnitude, magnitude) : 0);
                transform.position += offset;
                yield return new WaitForEndOfFrame();
                transform.position -= offset;
            }
        }
    }

    /// <summary>
    /// Smooth moves from current position to the target position.
    /// </summary>
    /// <returns>The move to.</returns>
    /// <param name="transform">Transform to be moved.</param>
    /// <param name="fromPos">Initial position of the transform.</param>
    /// <param name="toPos">Final position of the transform.</param>
    /// <param name="duration">Lerp duration.</param>
    /// <param name="myActions">List of actions to be executed at the end of the coroutine. Send a null if no actions are to be sent.</param>
    public static IEnumerator SmoothMove(Transform transform, Vector3 fromPos, Vector3 toPos, float duration, List<Action> myActions = null)
    {
        float elapsed = 0;
        while (elapsed < duration && transform)
        {
            transform.position = Vector3.Lerp(fromPos, toPos, (elapsed / duration));
            elapsed += Time.deltaTime;
            yield return null;
        }
        transform.position = toPos;
        if (myActions != null)
        {
            foreach (var Action in myActions)
            {
                Action();
            }
        }
        yield return null;
    }

    public static IEnumerator SmoothFade(GameObject go, Color toColor, float duration, List<Action> myActions = null)
    {
        float elapsed = 0;
        if (go.GetComponent<Image>())
        {
            Image image = go.GetComponent<Image>();
            Color startColor = image.color;
            while (elapsed < duration && go)
            {
                image.color = Color.Lerp(startColor, toColor, (elapsed / duration));
                elapsed += Time.deltaTime;
                yield return null;
            }
            image.color = toColor;
        }
        else if (go.GetComponent<SpriteRenderer>())
        {
            SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
            Color startColor = sr.color;
            while (elapsed < duration && go)
            {
                sr.color = Color.Lerp(startColor, toColor, (elapsed / duration));
                elapsed += Time.deltaTime;
                yield return null;
            }
            sr.color = toColor;
        }
        else if (go.GetComponent<TextMesh>())
        {
            TextMesh tm = go.GetComponent<TextMesh>();
            Color startColor = tm.color;
            while (elapsed < duration && go)
            {
                tm.color = Color.Lerp(startColor, toColor, (elapsed / duration));
                elapsed += Time.deltaTime;
                yield return null;
            }
            tm.color = toColor;
        }
        else
        {
            Debug.Log("Image/Sprite Rendere not found! Fade won't work.");
        }
        if (myActions != null)
        {
            foreach (var Action in myActions)
            {
                Action();
            }
        }
        yield return null;
    }

    //------------------------------------------------------------------------------------------------------------------------------LIST------------------------------------------------------------------------------------------------------------------------------//
    private static System.Random rng = new System.Random();
    /// <summary>
    /// Shuffle the specified list.
    /// </summary>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    /// <summary>
    /// Better than regular shuffle.
    /// </summary>
    /// <param name="a">The alpha component.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static void FYShuffle<T>(this IList<T> a)
    {
        // Loops through array
        for (int i = a.Count - 1; i > 0; i--)
        {
            // Randomize a number between 0 and i (so that the range decreases each time)
            int rnd = UnityEngine.Random.Range(0, i);
            // Save the value of the current i, otherwise it'll overwrite when we swap the values
            T temp = a[i];
            // Swap the new and old values
            a[i] = a[rnd];
            a[rnd] = temp;
        }
    }

    /// <summary>
    /// Sorts the number list (int).
    /// </summary>
    /// <param name="list">List to sort.</param>
    /// <param name="ascending">If set to true; sort in ascending order, else sort in descending order.</param>
    public static void SortNumberList(List<int> list, bool ascending = true)
    {
        int temp;
        for (int i = 0; i < list.Count - 1; i++)
        {
            for (int j = i + 1; j < list.Count; j++)
            {
                if (ascending)
                {
                    if (list[i] > list[j])
                    {
                        temp = list[i];
                        list[i] = list[j];
                        list[j] = temp;
                    }
                }
                else
                {
                    if (list[i] < list[j])
                    {
                        temp = list[i];
                        list[i] = list[j];
                        list[j] = temp;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Sorts the number list (float).
    /// </summary>
    /// <param name="list">List to sort.</param>
    /// <param name="ascending">If set to true; sort in ascending order, else sort in descending order.</param>
    public static void SortNumberList(List<float> list, bool ascending = true)
    {
        float temp;
        for (int i = 0; i < list.Count - 1; i++)
        {
            for (int j = i + 1; j < list.Count; j++)
            {
                if (ascending)
                {
                    if (list[i] > list[j])
                    {
                        temp = list[i];
                        list[i] = list[j];
                        list[j] = temp;
                    }
                }
                else
                {
                    if (list[i] < list[j])
                    {
                        temp = list[i];
                        list[i] = list[j];
                        list[j] = temp;
                    }
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------------------------------COLOR------------------------------------------------------------------------------------------------------------------------------//




    /// <summary>
    /// Returns the color with the desired alpha level
    /// </summary>
    /// <returns>Alpha value</returns>
    /// <param name="color">Color whose alpha is to be changed.</param>
    /// <param name="alpha">Alpha value between 0 and 1.</param>
    public static Color WithAlpha(this Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, alpha);
    }

    //the 4 methods below are taken from http://www.quickfingers.net/quick-bites-01-color-extensions/
    /// 
    /// Output a hex string from a color
    /// 
    ///
    /// Set to true to include a # character at the start
    /// 
    public static string HexFromColor(this Color color, bool includeHash = false)
    {
        string red = Mathf.FloorToInt(color.r * 255).ToString("X2");
        string green = Mathf.FloorToInt(color.g * 255).ToString("X2");
        string blue = Mathf.FloorToInt(color.b * 255).ToString("X2");
        return (includeHash ? "#" : "") + red + green + blue;
    }

    /// 
    /// Create a Color object from a Hex string (It's not important if you have a # character at
    /// the start or not)
    /// 
    /// The hex string to convert
    /// A Color object
    public static Color ColorFromHex(string color)
    {
        // remove the # character if there is one.
        color = color.TrimStart('#');
        float red = (HexToInt(color[1]) + HexToInt(color[0]) * 16f) / 255f;
        float green = (HexToInt(color[3]) + HexToInt(color[2]) * 16f) / 255f;
        float blue = (HexToInt(color[5]) + HexToInt(color[4]) * 16f) / 255f;
        Color finalColor = new Color { r = red, g = green, b = blue, a = 1 };
        return finalColor;
    }

    /// 
    /// Create a color object from integer R G B (A) components
    /// 
    ///The red component
    ///The green component
    ///The blue component
    ///The alpha component (Defaults to 255, or fully opaque)
    /// A Color object
    public static Color ColorFromInt(int r, int g, int b, int a = 255)
    {
        return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
    }

    private static int HexToInt(char hexValue)
    {
        return int.Parse(hexValue.ToString(), System.Globalization.NumberStyles.HexNumber);
    }

    public static Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

    public static Color ChangeColorBrightness(Color color, float correctionFactor)
    {
        float red = (float)color.r;
        float green = (float)color.g;
        float blue = (float)color.b;
        if (correctionFactor < 0)
        {
            correctionFactor = 1 + correctionFactor;
            red *= correctionFactor;
            green *= correctionFactor;
            blue *= correctionFactor;
        }
        else
        {
            red = (255 - red) * correctionFactor + red;
            green = (255 - green) * correctionFactor + green;
            blue = (255 - blue) * correctionFactor + blue;
        }
        return FromArgb((int)color.a, (int)red, (int)green, (int)blue);
    }

    public static Color FromArgb(int alpha, int red, int green, int blue)
    {
        //      float fa = ((float)alpha) / 255.0f;
        float fa = 255.0f;
        float fr = ((float)red) / 255.0f;
        float fg = ((float)green) / 255.0f;
        float fb = ((float)blue) / 255.0f;
        return new Color(fr, fg, fb, fa);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//


    //------------------------------------------------------------------------------------------------------------------------------VECTOR------------------------------------------------------------------------------------------------------------------------------//

    /// <summary>
    /// Truncates the Vector3 (z value is removed)
    /// </summary>
    /// <returns>Vector2.</returns>
    /// <param name="vec3">Vector3 to truncate.</param>
    public static Vector2 ToVec2(this Vector3 vec3)
    {
        return new Vector2(vec3.x, vec3.y);
    }


    /// <summary>
    /// Converts a Vector2 to Vector3 with the required z value
    /// </summary>
    /// <returns>Vector3.</returns>
    /// <param name="vec2">Vector2 to convert to Vector 3.</param>
    /// <param name="zValue">z	 value.</param>
    public static Vector2 ToVec3(this Vector2 vec2, float zValue)
    {
        return new Vector3(vec2.x, vec2.y, zValue);
    }

    /// <summary>
    /// Replaces the z value of the of the Vector3
    /// </summary>
    /// <returns>Vector3 whose z value is to be changed.</returns>
    /// <param name="vec3">Vector3.</param>
    /// <param name="zVal">z value.</param>
    public static Vector3 ToVec3(this Vector3 vec3, float zVal)
    {
        return new Vector3(vec3.x, vec3.y, zVal);
    }

    public static float GetAngleWithXAxis(Vector3 t1, Vector3 t2)
    {
        Vector3 distance = t2 - t1;
        float angle = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
        if (angle < 0)
        {
            angle += 360;
        }

        return angle;
    }

    public static float GetAngle(Vector2 p1, Vector2 pMid, Vector2 p2)
    {
        Vector2 vBA = (pMid - p1);
        Vector2 vCA = (pMid - p2);
        return Vector2.Angle(vBA, vCA);
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//


    public static void Width(this Transform tr, float widthMultiplier)
    {
        float screenHeight = Camera.main.orthographicSize * 2;
        float screenWidth = (screenHeight) / UnityEngine.Screen.height * UnityEngine.Screen.width;
        if (tr.GetComponent<SpriteRenderer>())
        {
            SpriteRenderer sr = tr.GetComponent<SpriteRenderer>();
            tr.transform.localScale = new Vector3((screenWidth / sr.sprite.bounds.size.x) * widthMultiplier, tr.transform.localScale.y, 1);
        }
        if (tr.GetComponent<MeshRenderer>())
        {
            MeshRenderer mr = tr.GetComponent<MeshRenderer>();
            tr.transform.localScale = new Vector3((screenWidth / mr.bounds.size.x) * widthMultiplier, tr.transform.localScale.y, 1);
        }

    }

    public static void Height(this Transform tr, float heightMultiplier)
    {
        float screenHeight = Camera.main.orthographicSize * 2;
        float screenWidth = (screenHeight) / UnityEngine.Screen.height * UnityEngine.Screen.width;
        if (tr.GetComponent<SpriteRenderer>())
        {
            SpriteRenderer sr = tr.GetComponent<SpriteRenderer>();
            tr.transform.localScale = new Vector3(tr.transform.localScale.x, (screenHeight / sr.sprite.bounds.size.y) * heightMultiplier, 1);

        }
        if (tr.GetComponent<MeshRenderer>())
        {
            MeshRenderer mr = tr.GetComponent<MeshRenderer>();
            tr.transform.localScale = new Vector3(tr.transform.localScale.x, (screenHeight / mr.bounds.size.y) * heightMultiplier, 1);
        }
    }




    /// <summary>
    /// Regular expression, which is used to validate an E-Mail address.
    /// </summary>
    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
          + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


    /// <summary>
    /// Checks whether the given Email-Parameter is a valid E-Mail address.
    /// </summary>
    /// <param name="email">Parameter-string that contains an E-Mail address.</param>
    /// <returns>True, wenn Parameter-string is not null and contains a valid E-Mail address;
    /// otherwise false.</returns>
    public static bool IsEmail(string email)
    {
        if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
        else return false;
    }


    //____________

    public static bool ValidateEmail(string email)
    {
        Regex regexEmail = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
        Match emailMatch = regexEmail.Match(email);
        if (emailMatch.Success)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool ValidateUsername(string _username)
    {
        Regex username = new Regex("^[a-zA-Z0-9 #!@_]+$");
        Match usernameMatch = username.Match(_username);
        if (usernameMatch.Success)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static bool ValidatePassword(string _password)
    {
        Regex password = new Regex("^[a-zA-Z0-9]+$");
        Match passwordMatch = password.Match(_password);
        if (passwordMatch.Success)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static bool ValidateAge(string _age)
    {
        Regex regexAge = new Regex("[0-9]");
        Match ageMatch = regexAge.Match(_age);
        if (ageMatch.Success)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

