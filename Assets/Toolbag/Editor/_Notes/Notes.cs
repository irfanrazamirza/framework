﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Notes : EditorWindow 
{

	string[] options;

    [MenuItem( "[Custom Tools]/Notes" )]
    static void Init()
    {
		var window = (Notes)EditorWindow.GetWindow( typeof( Notes ) );
        window.Show();
//        EditorPrefs.DeleteAll();
	
    }

    void OnApplicationQuit()
    {
    	Save();
    }
    void OnDisable()
    {
  	 	Save();
    }
    void OnEnable()
    {

		myText = EditorPrefs.GetString("savedText"+noteTitle, "");
		options = EditorPrefs.GetString("savedNotes", "").Split(new char[]{','});

    }

	Vector2 scrollBars;
    GUIStyle style;
	string myText;
	string noteTitle;
	int selected = 0;

	public void OnGUI()
    {	
    	Color defColor = GUI.color;
    	GUIStyle label = new GUIStyle(GUI.skin.box);
		label.wordWrap = true;


		options = EditorPrefs.GetString("savedNotes", "").Split(new char[]{','});

		GUI.color = Color.yellow;
		EditorGUILayout.LabelField("ALPHA, INCREDIBLY BUGGY!", label);
		GUI.color = defColor;

		EditorGUILayout.LabelField("To create a new note, save the current note, enter a title, and start typing!", label);

		EditorGUILayout.BeginHorizontal(GUI.skin.box);

		var btnSave = GUILayout.Button("Save", new GUIStyle(GUI.skin.button));
		var btnLoad = GUILayout.Button("Load", new GUIStyle(GUI.skin.button));

		if(options.Length>1)
		selected = EditorGUILayout.Popup(selected, options); 
		GUIStyle textStyle = new GUIStyle(GUI.skin.textField);
		noteTitle = EditorGUILayout.TextField(noteTitle, textStyle);

		EditorGUILayout.EndHorizontal();

		if(btnLoad)
		{	
			noteTitle = options[selected];

			if(EditorPrefs.HasKey("savedText"+noteTitle))
			{
				myText = EditorPrefs.GetString("savedText"+noteTitle, "");
				Debug.Log("Loaded an already saved note!");
			}

			else
			{
				Debug.Log("No note with title <color=red>"+noteTitle+"</color> found. Please create a new note.");
				myText = "";
			}
	
			Repaint();
		}

		if(btnSave)
		{
			if(noteTitle.Length>0)
			{
				Save();
				Debug.Log("Note saved as: <color=red>"+noteTitle+"</color>");
			}
		}

		style = new GUIStyle(GUI.skin.textArea);
        style.fontSize = 12;

        scrollBars =EditorGUILayout.BeginScrollView(scrollBars, true, true);
		myText = EditorGUILayout.TextArea(myText, style, GUILayout.MaxHeight(position.height));
        EditorGUILayout.EndScrollView();
	}

	void Save()
	{

		if(!EditorPrefs.HasKey("savedText"+noteTitle))
		{
			EditorPrefs.SetString("savedNotes", EditorPrefs.GetString("savedNotes")+","+noteTitle);
		}

		EditorPrefs.SetString("savedText"+noteTitle, myText);	
	}
}

