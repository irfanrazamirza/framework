﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class ImageUsage : EditorWindow 
{
	[MenuItem("Assets/Check For Usage")]
	public static void Find()
	{
		var fileName = Selection.activeObject.name;

		List <GameObject> foundObjects = new List<GameObject>();
		foreach (GameObject obj in Object.FindObjectsOfType(typeof(GameObject)))
		{
			if(obj.GetComponent<SpriteRenderer>())
			{
				if(obj.GetComponent<SpriteRenderer>().sprite.name == fileName)
				{
					foundObjects.Add(obj);
					Debug.Log(obj.name);
				}
			}

			if(obj.GetComponent<Image>())
			{
				if(obj.GetComponent<Image>().sprite.name == fileName)
				{
					foundObjects.Add(obj);
					Debug.Log(obj.name);
				}
			}
		}

		if (foundObjects.Count>0) 
		{
			GameObject[] foundArray = new GameObject[foundObjects.Count];
			for (int i = 0; i < foundObjects.Count; i++) {
				foundArray [i] = foundObjects [i];
			}
			
			Selection.objects = foundArray;
		}

		else
		{
			Debug.Log("No object found using "+fileName);
		}
	}
}