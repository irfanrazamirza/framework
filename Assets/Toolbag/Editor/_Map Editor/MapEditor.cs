﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MapEditor : EditorWindow 
{
	GameObject[] prefabs;
	GameObject selectedPrefab;
	List<GameObject> spawnedGO = new List<GameObject>();

	GUIContent handleContent = new GUIContent();

	[MenuItem("[Custom Tools]/Map Editor")]
	static void Init()
	{

		var window = (MapEditor)GetWindow(typeof(MapEditor), false, "Map Editor");
		window.position = new Rect(window.position.xMin + 100f, window.position.yMin + 100f, 500f, 250f);
	}


	void OnDisable()
	{
		SceneView.onSceneGUIDelegate -= OnSceneGUI;

	}


	Vector2 scrollPos;
	void OnGUI()
	{

		//Load all prefabs as objects from the 'Prefabs' folder
		Object[] obj =  Resources.LoadAll ("Prefabs",typeof(GameObject));
		if(obj.Count()==0)
		{
			GUIStyle label = new GUIStyle(GUI.skin.box);

			label.wordWrap = true;
			Color tempColor = GUI.color;
			GUI.color = Color.yellow;
			GUILayout.Label("Put tile prefabs in Resources>Prefabs.", label);
			GUI.color = tempColor;
			return;
		}
		//initialize the game object array
		prefabs = new GameObject[obj.Length];

		//store the game objects in the array
		for(int i=0;i<obj.Length;i++)
		{
			prefabs[i]=(GameObject)obj[i];
		}

		scrollPos = EditorGUILayout.BeginScrollView(scrollPos,false, false, GUILayout.Width(position.width), GUILayout.Height(position.height));
		GUILayout.BeginHorizontal ();
        if(prefabs!=null)
        {
            int elementsInThisRow=0;

            for( int i=0; i<prefabs.Length; i++)
            {
                elementsInThisRow++;

                //get the texture from the prefabs
                Texture prefabTexture = prefabs[i].GetComponent<SpriteRenderer> ().sprite.texture;

                //create one button for earch prefabs 
                //if a button is clicked, select that prefab and focus on the scene view
                if(GUILayout.Button(prefabTexture,GUILayout.MaxWidth(50), GUILayout.MaxHeight(50)))
                {    
                    selectedPrefab = prefabs[i];
					handleContent.image = (Texture2D)selectedPrefab.GetComponent<SpriteRenderer>().sprite.texture;

					SceneView.onSceneGUIDelegate -= OnSceneGUI;

					SceneView.onSceneGUIDelegate += OnSceneGUI;
					EditorWindow.FocusWindowIfItsOpen<SceneView>();

				

                }

                //move to next row after creating a certain number of buttons so it doesn't overflow horizontally
                if(elementsInThisRow>UnityEngine.Screen.width/70)
                {
                    elementsInThisRow=0;
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal ();        
                }
            }
		}
		GUILayout.BeginHorizontal ();

        GUILayout.EndScrollView();
	}


//	int height=32; int width = 32;
	void OnSceneGUI(SceneView sceneView)
	{

		Handles.BeginGUI();

		Vector3 pos = Camera.current.transform.position;

		GUILayout.BeginHorizontal(GUI.skin.box);

		if(GUILayout.Button("<", GUILayout.MaxWidth(20), GUILayout.MaxHeight(20)))
		{
			GameObject[] goArray = Selection.gameObjects;
			for(int i=0; i<goArray.Length; i++)
			{
				float selectedGameObjectWidth = goArray[i].GetComponent<SpriteRenderer>().bounds.size.x;

				goArray[i].transform.localPosition = new Vector3(goArray[i].transform.position.x-((selectedGameObjectWidth/2)+(selectedGameObjectWidth/2)), goArray[i].transform.position.y);
			}
		}

		if(GUILayout.Button(">", GUILayout.MaxWidth(20), GUILayout.MaxHeight(20)))
		{
			GameObject[] goArray = Selection.gameObjects;
			for(int i=0; i<goArray.Length; i++)
			{
				float selectedGameObjectWidth = goArray[i].GetComponent<SpriteRenderer>().bounds.size.x;

				goArray[i].transform.localPosition = new Vector3(goArray[i].transform.position.x+(selectedGameObjectWidth/2)+(selectedGameObjectWidth/2), goArray[i].transform.position.y);
			}
		}


		if(GUILayout.Button("^", GUILayout.MaxWidth(20), GUILayout.MaxHeight(20)))
		{
			GameObject[] goArray = Selection.gameObjects;
			for(int i=0; i<goArray.Length; i++)
			{
				float selectedGameObjectHeight = goArray[i].GetComponent<SpriteRenderer>().bounds.size.y;
				
				goArray[i].transform.localPosition = new Vector3(goArray[i].transform.position.x, goArray[i].transform.position.y+(selectedGameObjectHeight/2)+(selectedGameObjectHeight/2));
			}
		}
		

		if(GUILayout.Button("v", GUILayout.MaxWidth(20), GUILayout.MaxHeight(20)))
		{
			GameObject[] goArray = Selection.gameObjects;
			for(int i=0; i<goArray.Length; i++)
			{
				float selectedGameObjectHeight = goArray[i].GetComponent<SpriteRenderer>().bounds.size.y;

				goArray[i].transform.localPosition = new Vector3(goArray[i].transform.position.x, goArray[i].transform.position.y-((selectedGameObjectHeight/2)+(selectedGameObjectHeight/2)));
			}
		}

		Handles.EndGUI();     
		GUILayout.EndHorizontal();

		GUILayout.BeginVertical(GUI.skin.box);
		Handles.BeginGUI();

		GUILayout.Box(handleContent, GUILayout.MinWidth(40), GUILayout.MinHeight(40));
		if(GUILayout.Button("Done", GUILayout.MaxWidth(40), GUILayout.MaxHeight(20)))
		{
			SceneView.onSceneGUIDelegate -= OnSceneGUI;
		}

		if(GUILayout.Button("Flip X", GUILayout.MaxWidth(40), GUILayout.MaxHeight(20)))
		{
			GameObject[] goArray = Selection.gameObjects;
			for(int i=0; i<goArray.Length; i++)
			{
				goArray[i].transform.localScale = new Vector2(goArray[i].transform.localScale.x*-1, goArray[i].transform.localScale.y);
			}
		}

		if(GUILayout.Button("Flip Y", GUILayout.MaxWidth(40), GUILayout.MaxHeight(20)))
		{
			GameObject[] goArray = Selection.gameObjects;
			for(int i=0; i<goArray.Length; i++)
			{
				goArray[i].transform.localScale = new Vector2(goArray[i].transform.localScale.x, goArray[i].transform.localScale.y*-1);
			}
		}

		Handles.EndGUI();     
		GUILayout.EndVertical();




		Vector3	spawnPosition = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition).origin;

	    //if 'E' pressed, spawn the selected prefab
        if (Event.current.type==EventType.KeyDown &&Event.current.keyCode == KeyCode.E) 
        {
 			Spawn(spawnPosition);
        }

		//if 'R' pressed, spawn the selected prefab
        if (Event.current.type==EventType.KeyDown &&Event.current.keyCode == KeyCode.R) 
        {
			GameObject[] goArray = Selection.gameObjects;
			for(int i=0; i<goArray.Length; i++)
			{
				goArray[i].transform.eulerAngles = new Vector3(goArray[i].transform.eulerAngles.x, goArray[i].transform.eulerAngles.y,goArray[i].transform.eulerAngles.z-90);
			}
        }


		//if 'X' is pressed, undo (remove the last spawned prefab)
		if (Event.current.type==EventType.KeyDown &&Event.current.keyCode == KeyCode.X) 
		{	
			if(spawnedGO.Count>0)
			{
				if(spawnedGO[spawnedGO.Count-1]!=null)
				{
					DestroyImmediate(spawnedGO[spawnedGO.Count-1]);
				}
				spawnedGO.RemoveAt(spawnedGO.Count-1);
			}
		}


		if(selectedGameObject!=null)
        {
            Handles.Label(selectedGameObject.transform.position, "X");

        }

	

		if(selectedPrefab!=null)
		{
			if(selectedGameObject!=null && selectedGameObject.GetComponent<SpriteRenderer>())
			{
				float selectedGameObjectWidth = selectedGameObject.GetComponent<SpriteRenderer>().bounds.size.x;
				float selectedGameObjectHeight = selectedGameObject.GetComponent<SpriteRenderer>().bounds.size.y;

				float selectedPrefabWidth = selectedPrefab.GetComponent<SpriteRenderer>().bounds.size.x;
				float selectedPrefabHeight = selectedPrefab.GetComponent<SpriteRenderer>().bounds.size.y;

				if (Event.current.type==EventType.KeyDown &&Event.current.keyCode == KeyCode.W) 
				{
					spawnPosition = new Vector3(selectedGameObject.transform.position.x, selectedGameObject.transform.position.y+(selectedGameObjectHeight/2)+(selectedPrefabHeight/2), 0);
					Spawn(spawnPosition);
				}
				if (Event.current.type==EventType.KeyDown &&Event.current.keyCode == KeyCode.S) 
				{
					spawnPosition = new Vector3(selectedGameObject.transform.position.x, selectedGameObject.transform.position.y-(selectedGameObjectHeight/2)-(selectedPrefabHeight/2), 0);
					Spawn(spawnPosition);
				}
				if (Event.current.type==EventType.KeyDown &&Event.current.keyCode == KeyCode.A) 
				{
					spawnPosition = new Vector3(selectedGameObject.transform.position.x-(selectedGameObjectWidth/2)-(selectedPrefabWidth/2), selectedGameObject.transform.position.y, 0);
					Spawn(spawnPosition);
				}
				if (Event.current.type==EventType.KeyDown &&Event.current.keyCode == KeyCode.D) 
				{
					spawnPosition = new Vector3(selectedGameObject.transform.position.x+(selectedGameObjectWidth/2)+(selectedPrefabWidth/2), selectedGameObject.transform.position.y, 0);
					Spawn(spawnPosition);
				}
	
			}
		}

		if(Selection.activeGameObject!=null)
		{
			selectedGameObject = Selection.activeGameObject;
		}

		SceneView.RepaintAll();
	}

	GameObject selectedGameObject;
	void Spawn(Vector2 _spawnPosition)
    {
        GameObject go = (GameObject)Instantiate(selectedPrefab,new Vector3((_spawnPosition.x), (_spawnPosition.y), 0), selectedPrefab.transform.rotation);
        Selection.activeObject = go;

		go.name = selectedPrefab.name;
        
        spawnedGO.Add(go);
        if(spawnedGO.Count>49)
        {
        	spawnedGO.RemoveAt(0);
        }
    }

	public float gridSpacingX = 0.5f;
    public float gridSpacingY = 0.5f;
      
    public int blocksWide =32;
    public int blocksHigh = 24;
      
    public Vector3 gridCentre;
     
      
    Vector3 GetLowerLeftDrawPoint(int squaresWide , int squaresHigh ) // returns the starting point at the lower left point
    {
        float xPos =(float)( -((squaresWide * gridSpacingX)/2) + gridCentre.x);
        float yPos = (float)(-((squaresHigh * gridSpacingY)/2) + gridCentre.y);
        return new Vector3 (xPos, yPos, 0f);
    }

	void OnDrawGizmos () 
    {
    	Debug.Log("df");
        Gizmos.color = Color.grey;
        Vector3 lowerLeftStartPoint = GetLowerLeftDrawPoint(blocksWide, blocksHigh);
         
        float lineLength = blocksWide * gridSpacingX;
        for( int xLine = 0 ; xLine <= blocksHigh ; xLine++)
        {
           Vector3 vFrom =  lowerLeftStartPoint + new Vector3(0f,(float)(xLine * gridSpacingY), 0f);
           Vector3 vTo = vFrom + new Vector3(lineLength, 0f, 0f);
           Gizmos.DrawLine (vFrom, vTo);
        }

        // draw the vertical lines now
        lineLength = blocksHigh * gridSpacingY;
        for (int yLine = 0; yLine <= blocksWide ; yLine++)
        {
           Vector3 hFrom = lowerLeftStartPoint + new Vector3((float)(yLine * gridSpacingX), 0f, 0f);
           Vector3 hTo = hFrom + new Vector3(0f, lineLength, 0f);
           Gizmos.DrawLine (hFrom, hTo);
        }

         
    }
}
