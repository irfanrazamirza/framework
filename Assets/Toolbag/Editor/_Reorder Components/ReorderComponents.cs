﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class ReorderComponents : EditorWindow 
{

	[MenuItem("[Custom Tools]/Reorder Components")]
	static void LaunchWindow()
	{
		ReorderComponents window = (ReorderComponents)EditorWindow.GetWindow(typeof(ReorderComponents));
		GUIContent titleContent = new GUIContent ("Reorder");
		window.titleContent = titleContent;

	}


	//list to cache all the components
	List<Component>componentList =new List<Component>();

	//Callback triggered when selection is changed in Unity Editor
	//Basically, everytime you select some other game object, this method gets called.
	void OnSelectionChange()
	{
		if(Selection.activeGameObject!=null)
		{

			//Get all the components of in the selected Gameobject
			Component[] components = Selection.activeGameObject.GetComponents<Component>() as Component[];

			//clear the component list
			componentList.Clear();

			//null check
			if(components.Length > 0) 
			{
				//set number of components and add components to the list we just cleared
	           	foreach(Component component in components)
	           	{
					componentList.Add(component);
	           	}
	        }	
		}

		Repaint();
    }

	int repaintType = 0;
	Vector2 scrollPos;
	void OnGUI()
	{
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false,false,  GUILayout.Width(position.width), GUILayout.Height(position.height));

		//if no Game Object is selected or the selected Game Object less than 1 component (transform)
		if(componentList.Count<2)
		{
			GUIStyle label = new GUIStyle(GUI.skin.box);

			label.wordWrap = true;
			GUI.color = Color.yellow;
			GUILayout.Label("Select a Game Object with at least 2 components.", label);
		}

		else
		{
			repaintType = GUILayout.Toolbar(repaintType, new string[]{"Static", "Dynamic"});

		}

		//we start the loop from 1 and not 0 because the first component, i.e, the transform component cannot be removed or moved
		for(int i=1; i<componentList.Count;i++)
		{

			EditorGUILayout.BeginVertical();
			EditorGUILayout.BeginHorizontal(GUI.skin.box);

			GUILayout.Label(componentList[i].GetType().Name, GUILayout.MinWidth(position.width-150));

			GUILayout.FlexibleSpace();
			Color tempColor = GUI.color;
			GUI.color = Color.green;

			//Create 2 buttons for every component, one to move it up and another to move it down.
			if(GUILayout.Button("^", GUILayout.MaxWidth(40), GUILayout.MinWidth(40)))
			{
				 UnityEditorInternal.ComponentUtility.MoveComponentUp(componentList[i]);
				 if(repaintType == 1)
				 OnSelectionChange();
				
			}

			GUI.color = Color.cyan;
			if(GUILayout.Button("v", GUILayout.MaxWidth(40), GUILayout.MinWidth(40)))
			{
				UnityEditorInternal.ComponentUtility.MoveComponentDown(componentList[i]);
				if(repaintType == 1)
				OnSelectionChange();
			
			}

			GUI.color = tempColor;
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndVertical();
		}

		EditorGUILayout.EndScrollView();

		//repaint so the buttons are updated with the correct names in the correct order if the dynamic option is selected
		if(GUI.changed && repaintType == 1)
		{
			OnSelectionChange();
		}
		
	}

	void OnFocus()
	{
		OnSelectionChange();
	}



}
