﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class BulkRename : EditorWindow
{
	
	string prefix;
	string infix;
	string previewField;
	bool hasIndex;
	[MenuItem("GameObject/[Custom Tools]/Bulk Rename #3", false, 103)]
	static void LaunchBulkRenameWindow() 
	{
		BulkRename window = (BulkRename)EditorWindow.GetWindow (typeof(BulkRename));
		window.title = "Bulk Rename";

	}

	void OnGUI()
	{	
		EditorGUILayout.BeginVertical();

		prefix = EditorGUILayout.TextField("Name: ", prefix);
		infix = EditorGUILayout.TextField("Infix: ", infix);
		hasIndex = EditorGUILayout.Toggle("Insert Index?", hasIndex);
		EditorGUILayout.Space();

		if(hasIndex)
		EditorGUILayout.LabelField("Preview: "+prefix+""+infix+""+("#"), previewField);
		else
		EditorGUILayout.LabelField("Preview: "+prefix+""+infix, previewField);
		
		EditorGUILayout.Space();
		
		var renameButton = GUILayout.Button("Rename!");

		if(renameButton)
		{
			for(int i=0; i<Selection.transforms.Length; i++)
			{
				if(hasIndex)
				{
					Selection.transforms[i].name = prefix+""+infix+""+(i+1);
				}
				else
				{
					Selection.transforms[i].name = prefix+""+infix;
				
				}
			}
				

			this.Close();

			EditorGUILayout.EndVertical();
		}
	}
}